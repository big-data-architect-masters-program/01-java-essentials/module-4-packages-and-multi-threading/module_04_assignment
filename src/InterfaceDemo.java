public class InterfaceDemo implements Queue {

    @Override
    public void insert() {
        System.out.println("This is insert function from Interface.");
    }

    @Override
    public void delete() {
        System.out.println("This is delete function from Interface.");
    }
}

interface Queue{
    void insert();
    void delete();
}
