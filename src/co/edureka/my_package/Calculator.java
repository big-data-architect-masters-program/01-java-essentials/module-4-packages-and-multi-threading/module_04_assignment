package co.edureka.my_package;

public class Calculator {
    public void subtract(int a, int b) {
        System.out.println("subtract: " + (a - b));
    }
    public void multiply(int a, int b) {
        System.out.println("multiply: " + (a * b));
    }
    public void divide(int a, int b) {
        System.out.println("divide: " + (a / b));
    }
    public void factorial(int n) {
        long fact = 1;
        for (int i = 2; i <= n; i++) {
            fact = fact * i;
        }
        System.out.println("factorial: " + fact);
    }
    public void reverse(int num) {
        int reversed = 0;

        while(num != 0) {
            int digit = num % 10;
            reversed = reversed * 10 + digit;
            num /= 10;
        }

        System.out.println("reverse: " + reversed);
    }

}
