import co.edureka.my_package.Calculator;
public class PackageDemo {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.subtract(4,5);
        calculator.multiply(3,4);
        calculator.divide(4,2);
        calculator.factorial(3);
        calculator.reverse(54321);
    }

}
